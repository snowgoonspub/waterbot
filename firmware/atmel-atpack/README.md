# Atmel ATpack
These packs describe the specific microcontroller capabilities, and
are critical for compiling/linking for our target device.

The works in this directory are Copyright (c) 2020 Microchip Technology Inc.

They are included herein in accordance with the terms of the Apache License 2.0.

## Why include these here?
We need to ensure consistent builds across environments (in particular,
in our Continuous Integration environment.)  Asking for magic files to
be installed in magic places is not compatible with that goal.  Equally,
neither is downloading the files from a URL which may change over time.

The original files are available from [Microchip Technology's Website](http://packs.download.atmel.com).

