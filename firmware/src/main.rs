#![no_std]
#![no_main]

mod data;

use avr_oxide::alloc::boxed::Box;
use avr_oxide::hal::atmega4809::hardware;
use avr_oxide::arduino;
use avr_oxide::devices::{UsesPin, OxideLed, OxideButton, Handle};


// Code ======================================================================


/**
 * Main function demonstrating the simple on_event based approach to handling
 * device IO.
 */
#[avr_oxide::main(chip="atmega4809")]
pub fn main() -> ! {
  let supervisor = avr_oxide::oxide::instance();

  // Get access to our hardware
  let hardware = hardware::instance();

  // Translate it into the more familiar Arduino pin naming
  let arduino = arduino::nanoevery::Arduino::from(hardware);

  let button = Handle::new(OxideButton::with_pin(arduino.d5).pullup(true));
  let green = Handle::new(OxideLed::with_pin(arduino.d7));

  // Set an event handler to be called every time someone presses the button
  button.on_click(Box::new(move |_pinid, _state|{
    green.toggle();
  }));

  // Tell the supervisor what devices to pay attention to, and then enter
  // the main loop.
  supervisor.listen_handle(button);
  supervisor.run();
}