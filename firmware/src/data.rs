/* data.rs
 *
 * Developed by Tim Walls <tim.walls@snowgoons.com>
 * Copyright (c) All Rights Reserved, Tim Walls
 */
//! Data structures for our plant watering robot.
//! 

// Imports ===================================================================
use avr_oxide::time::Duration;
use avr_oxide::util::persist::derive::Persist;
use ufmt::derive::uDebug;

// Declarations ==============================================================

/**
 * Possible watering time values:
 * 0 == Do not water
 * 1 == 15 seconds
 * 2 == 30 seconds
 * 3 == 1 minute
 * 4 == 2 minutes
 * 5 == 4 minutes
 */
#[derive(Persist,uDebug)]
pub struct WateringTime(u8);

/**
 * Possible sleep time values
 * 0  == Forever
 * 1  == 15 minutes
 * 2  == 30 minutes
 * 3  == 1 hour
 * 4  == 3 hours
 * 5  == 6 hours
 * 6  == 12 hours
 * 7  == 24 hours
 * 8  == 2 days
 * 9  == 4 days
 * 10 == 7 days
 */
#[derive(Persist,uDebug)]
pub struct SleepTime(u8);


/// Any value which can be manipulated by our incredibly simple two-buttons,
/// one LED, user interface.
pub trait SimpleUiValue {
  fn get_index_value(&self) -> u8;

  /// Increment the value.  Do nothing if we are already at the maximum
  fn increment_value(&mut self);

  /// Decrement the value.  Do nothing if we are already at the maximum.
  fn decrement_value(&mut self);

  /// True iff the value can be incremented further.
  fn can_increment(&self) -> bool;

  /// True iff the value can be decremented further.
  fn can_decrement(&self) -> bool;
}


#[derive(Persist,Default,uDebug)]
#[persist(magicnumber = 1)]
struct Configuration {
  pub pump1_watering_time: WateringTime,
  pub pump1_sleep_time: SleepTime,
  pub pump2_watering_time: WateringTime,
  pub pump2_sleep_time: SleepTime
}

// Code ======================================================================
impl SimpleUiValue for WateringTime {
  fn get_index_value(&self) -> u8 {
    self.0
  }

  fn increment_value(&mut self) {
    if self.can_increment() {
      self.0 += 1;
    }
  }

  fn decrement_value(&mut self) {
    if self.can_decrement() {
      self.0 -= 1;
    }
  }

  fn can_increment(&self) -> bool {
    self.0 < 5
  }

  fn can_decrement(&self) -> bool {
    self.0 > 0
  }
}

impl Into<Duration> for WateringTime {
  fn into(self) -> Duration {
    match self.0 {
      1  => Duration::from_secs(  15u32 ),
      2  => Duration::from_secs(  30u32 ),
      3  => Duration::from_secs(  60u32 ),
      4  => Duration::from_secs(  2u32 * 60u32 ),
      5  => Duration::from_secs(  4u32 * 60u32 ),
      _ => Duration::MAX,
    }
  }
}

impl Default for WateringTime {
  fn default() -> Self {
    Self(0)
  }
}


impl SimpleUiValue for SleepTime {
  fn get_index_value(&self) -> u8 {
    self.0
  }

  fn increment_value(&mut self) {
    if self.can_increment() {
      self.0 += 1;
    }
  }

  fn decrement_value(&mut self) {
    if self.can_decrement() {
      self.0 -= 1;
    }
  }

  fn can_increment(&self) -> bool {
    self.0 < 10
  }

  fn can_decrement(&self) -> bool {
    self.0 > 0
  }
}

impl Into<Duration> for SleepTime {
  fn into(self) -> Duration {
    match self.0 {
      1  => Duration::from_secs(  15u32  * 60u32),
      2  => Duration::from_secs(  30u32  * 60u32),
      3  => Duration::from_secs(  60u32  * 60u32),
      4  => Duration::from_secs(  3u32 * 60u32  * 60u32),
      5  => Duration::from_secs(  6u32 * 60u32  * 60u32),
      6  => Duration::from_secs(  12u32 * 60u32  * 60u32),
      7  => Duration::from_secs(  24u32 * 60u32  * 60u32),
      8  => Duration::from_secs(  2u32 * 24u32 * 60u32  * 60u32),
      9  => Duration::from_secs(  4u32 * 24u32 * 60u32  * 60u32),
      10 => Duration::from_secs(  7u32 * 24u32 * 60u32  * 60u32),
      _ => Duration::MAX,
    }
  }
}

impl Default for SleepTime {
  fn default() -> Self {
    Self(0)
  }
}


// Tests =====================================================================
#[cfg(test)]
mod tests {
  #[allow(unused_imports)]
  use super::*;
  
  #[test]
  fn a_test() {
    
  }
  
  #[test]
  #[should_panic]
  fn a_failure_test() {
    panic!()
  }
}