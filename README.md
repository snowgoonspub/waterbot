# ApăCiclu
## What is it?
A plant watering robot!

#### Author/License
Designed/developed by Tim Walls <tim.walls@snowgoons.com>.
Copyright (c) 2021 Tim Walls, All Rights Reserved.

See [LICENSE.md](LICENSE.md) for copyright and licensing details.